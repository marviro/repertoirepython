from flask import Flask, render_template, redirect 
import links_from_header
import requests
import json

repertoire = Flask(__name__, template_folder='./templates/')

# fonction pour construire la home page
@repertoire.route('/') 
def home():
  r = requests.get(
      'http://repertoire.ecrituresnumeriques.ca/api/items')
  data=json.loads(r.text)
  headers=r.headers #request allows to extract the headers
  header=headers['Link'] # in the headers json there is a field Link
  linkheader=links_from_header.extract(header) # using links_from_headers to tranform Link value in a json with the keys prev next last
  lastlink=linkheader['last'] # picking the value of the key last 
  lastpage=int(lastlink.replace('http://repertoire.ecrituresnumeriques.ca/api/items?sort_by=id&sort_order=asc&page=', '')) # I need the number of the last page which is at the end of the string, so I delete the beginning of the string
  # print(str(lastpage))
  maxpage=lastpage+1 #to be sure to parse all the pages
  count=1
  while count < maxpage:
      print('ok')
      count=count+1
      r1 = requests.get('http://repertoire.ecrituresnumeriques.ca/api/items?sort_by=id&sort_order=asc&page='+str(count))
      data2=json.loads(r1.text)
      for i in data2:
          data.append(i)
  return render_template('home.html',data=data, headers=r1.headers)

if __name__ == '__main__':
  repertoire.run(host='0.0.0.0', debug=True)

  # exemple: http://repertoire.ecrituresnumeriques.ca/api/items?sort_by=dcterms:date
